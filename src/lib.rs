pub mod euler;

use euler::multiples_001;

pub fn entry() {
    multiples_001::main()
}
