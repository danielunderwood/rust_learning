//! The `multiples_001` module solves [the first Euler problem](https://projecteuler.net/problem=1)

use std::vec;

/// `main()` is the entry point for the module. It will solve the exact problem given in the text
pub fn main() {
	// Find multiples of 3 or 5 below 1000
	let multiples_of: [u64; 2] = [3, 5];
	let max = 1000;
	let ans = sum_multiples_below(&multiples_of, max);
	println!("{}: {}", file!(), ans);
}

/// Gets the multiples of `multiples_of` below `max`
///
/// # Examples
///
/// ```
/// use rust_learning::euler::multiples_001::multiples_below;
///
/// let multiples: [u64; 2] = [3,5];
/// let max = 10;
/// assert_eq!(vec![3, 5, 6, 9], multiples_below(&multiples, max));
/// ```
pub fn multiples_below(multiples_of: &[u64], max: u64) -> vec::Vec<u64> {
	// Create new vector
	let mut ret = vec::Vec::new();

	// Loop through numbers to max numbers
	'numbers: for num in 1..max {
		// Check if num if a multiple of any of the numbers in multiples_of
		'multiples: for multiple in multiples_of {
			if num % multiple == 0 {
				// Add and move into next num if matches
				ret.push(num);
				continue 'numbers;
			}
		}
	}

	ret
}

/// Finds the sum of the multiples of `multiples_of` below `max`
///
/// # Examples
///
/// ```
/// use rust_learning::euler::multiples_001::sum_multiples_below;
///
/// let multiples: [u64; 2] = [3,5];
/// let max = 10;
/// assert_eq!(23, sum_multiples_below(&multiples, max));
/// ```
pub fn sum_multiples_below(multiples_of: &[u64], max: u64) -> u64 {
	// Get terms that are multiples
	let terms = multiples_below(multiples_of, max);

	let mut sum = 0;
	// Increment sum
	for num in terms {
		sum += num;
	}

	sum
}
